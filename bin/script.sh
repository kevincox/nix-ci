#! @zsh@/bin/zsh

set -eux -o pipefail

eval "NIX_CI_ARGS=(${NIX_CI_ARGS:-})"

cmd=(
	@nix@/bin/nix-build
	/data/build.nix
	--option sandbox "$NIX_CI_SANDBOX"
	--show-trace
	--arg user-expression ./default.nix
	--argstr artifacts "$NIX_CI_ARTIFACTS"
	--extra-substituters "$NIX_CI_EXTRA_SUBSTITUTERS"
	--extra-trusted-public-keys "$NIX_CI_EXTRA_TRUSTED_PUBLIC_KEYS"
	"$NIX_CI_ARGS[@]"
)

if [[ -v NIX_CI_STORE ]]; then
	cmd+=(--eval-store local --store $NIX_CI_STORE)
fi

PATH=@nixpath@ "$cmd[@]"

if [[ -v NIX_CI_STORE ]]; then
	@nix@/bin/nix \
		--extra-experimental-features nix-command \
		copy --from $NIX_CI_STORE --no-check-sigs \
		$(realpath result)
	realpath result >>/tmp/nix-ci-built-paths
fi

@coreutils@/bin/cp -rL result/artifacts/* .
