#! @zsh@/bin/zsh

set -eu -o pipefail

if [[ -n ${CACHIX_SIGNING_KEY:-} ]]; then
	eval "CACHIX_REPO=(${CACHIX_REPO:-})"

	! realpath result >>/tmp/nix-ci-built-paths
	@gnugrep@/bin/grep -v '\.drv$' /tmp/nix-ci-built-paths | \
		PATH=@nix@/bin @cachix@/bin/cachix push -c9 $CACHIX_REPO[1]
fi
