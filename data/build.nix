{
	nixpkgs ? import <nixpkgs> {},
	user-expression,
	artifacts
}:

with nixpkgs; let
	user-evaluated = import user-expression {};
	derivations = lib.filterAttrs (k: v: lib.isDerivation v) user-evaluated;
	scripts = lib.mapAttrsToList (attr: derivation: ''
		echo ${derivation} >$out/public/${attr}.nixpath
		ln -s ${derivation} $out/results/${attr}
	'') derivations;
in
	pkgs.runCommand "nix-ci-result" {} ''
		mkdir -p $out/{artifacts/results,public,results}
		${lib.concatStringsSep "\n" scripts}

		cp -r $out/public $out/artifacts
		for a in ${artifacts}; do
			ln -sv "$out/results/$a" $out/artifacts/results/
		done
	''
