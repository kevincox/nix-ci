{
	nixpkgs ? import <nixpkgs> {},
}:
with nixpkgs; let
	nix-ci = pkgs.stdenv.mkDerivation {
		name = "nix-ci";
		src = ./bin;
		
		inherit (pkgs) cachix coreutils gnugrep nix zsh;

		# nix is naughty and doesn't declare all of its dependencies.
		nixpath = lib.makeBinPath [
			git
			gnutar
			gzip
			nix
			openssh
			xz
		];

		buildPhase = ''
			mkdir -p $out/bin
			for script in *; do
				substituteAll $script $out/bin/$script
				chmod +x $out/bin/$script
				echo $script
			done

			mkdir -p $out/etc/nix/
			substituteAll ${./etc/nix.conf} $out/etc/nix/nix.conf
		'';

		installPhase = ''
			mkdir $out/tmp

			mkdir -p $out/usr/bin/
			ln -s ${pkgs.coreutils}/bin/env $out/usr/bin/

			cp -r ${./data} $out/data

			install -D ${./etc/group} $out/etc/group
			install -D ${./etc/nsswitch.conf} $out/etc/nsswitch.conf
			install -D ${./etc/passwd} $out/etc/passwd
			install -D ${./etc/ssh_config} $out/etc/ssh/ssh_config
		'';
	};

	sh = pkgs.runCommand "sh" {} ''
		mkdir -p $out/bin
		ln -s ${zsh}/bin/zsh $out/bin/sh
	'';
in rec {
	docker = dockerTools.buildLayeredImage {
		name = "nix-ci";
		tag = "latest";
		contents = [
			cacert
			nix-ci
			sh
		];
		config = {
			Env = [
				"NIX_CI_ARTIFACTS="
				"NIX_CI_EXTRA_SUBSTITUTERS="
				"NIX_CI_EXTRA_TRUSTED_PUBLIC_KEYS="
				"NIX_CI_SANDBOX=true"
				"NIX_PATH=nixpkgs=https://nixos.org/channels/nixpkgs-unstable/nixexprs.tar.xz"
				"PATH=${coreutils}/bin:${gnugrep}/bin:${sh}/bin"
				"SSL_CERT_FILE=${cacert}/etc/ssl/certs/ca-bundle.crt"
				"USER=root"
			];
		};
	};
}
