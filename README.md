# GitLab CI for Nix

## Project Setup

1. Create a `default.nix` file that describes the derivations that you would like to be built.
2. Include this script.

	```yaml
	include:
	- remote: 'https://gitlab.com/kevincox/nix-ci/-/raw/v1/gitlab-ci.yml'
	```

## Results

nix-ci will build all of the attributes in your `default.nix` file and publish a GitLab Pages result with the paths. For each attribute built there will be a `public/{attrbiute}.nixpath` artifact created and it will be uploaded. Note that in order for that path to be fetched you will need to upload the result to a binary cache.

## Advanced Options

### Arguments

If you would like to pass arguments to the `nix-build` invocation you can do that by setting `NIX_CI_ARGS`. These are passed on the command line as is. So you will need to appropriately quote any values that you don't want interpreted by the shell. However you can also reference GitLab variables.

```yaml
variables:
  NIX_CI_ARGS: --arg release true --argstr version $CI_COMMIT_SHA
```

### Artifacts

If you would like to pass artifacts to later build steps you can set `NIX_CI_ARTIFACTS` to the attribute names of the artifacts. The result of building the specified attributes will be copied to `results/{attribute}`.

```yaml
variables:
  NIX_CI_ARTIFACTS: www www-zip
```

### Cachix

In order to avoid rebuilding the same packages again and again you can use a [Cachix](https://cachix.org/) cache. Just set the appropriate variables.

* `CACHIX_REPO`: Names of caches to use. (Required)
* `CACHIX_SIGNING_KEY`: Value signing key if you want to push the results. If specified the results will be pushed to the first cache in `CACHIX_REPO` (Optional)

## Runner Setup

nix-ci works fine with just about any GitLab runner. However, if you are using self-hosted runners you can use cache volumes to make startup quicker and cache results even without (or in addition to) a binary cache.

nix-ci can use a chroot-store located at `$NIX_CI_STORE`. For basic caching just create a cache volume at that location and pass `NIX_CI_STORE=/mnt`

```toml
# GitLab Runner Config
[[runners]]
	executor = "docker"
	environment = ["NIX_CI_STORE=/mnt"]
	[runners.docker]
		volumes = ["/mnt"]
```

To share the same cache for all runners use a bind mount instead. However, this may have problems with concurrent builds as the same store will be used by all jobs.

```toml
# GitLab Runner Config
[[runners]]
	executor = "docker"
	environment = ["NIX_CI_STORE=/mnt"]
	[runners.docker]
		volumes = ["/some/host/path:/mnt"]
```

If you have a Nix daemon running on the host machine you can get the best performance by bind-mounting the host store into the runner container. This allows the jobs to execute jobs via the daemon which allows fully concurrent access as well as scheduling parallelism at the machine-level rather than the job-level. Note that this means that daemon options need to be configured properly for the runner user. For example if you want to allow arbitrary binary caches the runner user needs to be listed in [`trusted-users`](https://nixos.org/manual/nix/stable/command-ref/conf-file.html).

```toml
# GitLab Runner Config
[[runners]]
	executor = "docker"
	environment = ["NIX_CI_STORE=unix:///mnt/nix/var/nix/daemon-socket/socket?root=/mnt"]
	[runners.docker]
		volumes = ["/nix:/mnt/nix:ro"]
```
